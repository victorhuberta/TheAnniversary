extends Node2D

signal cutscene_over
signal preload_music
signal save_game

signal BGMUSIC
signal BGMUSIC_STOP
signal BGSOUND
signal BGSOUND_STOP
signal SOUND
signal VISUAL
signal CHOICE
signal BRANCH
signal CHOICE_END

var cutscene_names = ["introduction", "who_am_i", "ohana_means_family", "gifts", "the_evil_spirit"]
var cutname_idx = 0
var cutscene
var cut_idx = 0
var tie

const BRANCH_0 = 0
const BRANCH_1 = 1
const BRANCH_2 = 2
var chosen_branch = BRANCH_0

func _ready():
	tie = get_node("StoryTIE")

func get_savedata():
	return cutname_idx

func set_savedata(loaded_idx):
	cutname_idx = loaded_idx

func load_savedata():
	play_story()

func play_story():
	_load_cutscene()
	_play_cutscene()

func _load_cutscene():
	if (cutname_idx >= cutscene_names.size()):
		return
	emit_signal("save_game", "Cutscene2D")

	var file = File.new()
	var filename = "res://cutscenes/%s.txt" % cutscene_names[cutname_idx]
	file.open(filename, File.READ)
	cutscene = file.get_as_text().split("\n")
	file.close()
	
	_preprocess_cutscene()

	cut_idx = 0
	cutname_idx += 1

func _preprocess_cutscene():
	for line in cutscene:
		if (line == ""): continue
		var sections = line.split("|")
		if (sections.size() > 1):
			if (sections[0] == "BGMUSIC"):
				emit_signal("preload_music", sections[1])

func _play_cutscene():
	_on_NextBtn_pressed()

func _on_NextBtn_pressed():
	if (cut_idx >= cutscene.size()):
		emit_signal("cutscene_over")
		return

	var line = cutscene[cut_idx]
	cut_idx += 1
	if (line == ""):
		_on_NextBtn_pressed()
		return

	var sections = line.split("|")
	if (sections.size() > 1):
		if (sections[0] == "CHOICE" or sections[0] == "BRANCH"):
			emit_signal(sections[0], sections[1], sections[2])
			return
		emit_signal(sections[0], sections[1])
		_on_NextBtn_pressed()
		return
	
	_write_text(line)

func _write_text(line):
	var speaker_name = ""
	var text = line

	var sections = line.split("::")
	if (sections.size() > 1):
		emit_signal("SOUND", "talk")
		speaker_name = sections[0]
		text = sections[1]
	
	get_node("SpeakerLbl").set_text(speaker_name)
	tie.reset()
	tie.buff_text(text, 0.01)
	tie.set_state(tie.STATE_OUTPUT)

func _on_Cutscene2D_VISUAL(visualname):
	var sprite = get_node("PictureASprite")
	if (visualname == "NONE"):
		sprite.set_hidden(true)
		return
	sprite.set_animation(visualname)

	var screen_size = get_viewport_rect().size
	var sprite_size = sprite.get_item_rect().size
	var a = 1.0
	if (sprite_size.x > sprite_size.y):
		a = screen_size.x / sprite_size.x
	else:
		a = screen_size.y / sprite_size.y
	sprite.set_scale(Vector2(a, a))
	sprite.set_hidden(false)
	sprite.play(visualname)

func _on_Branch1Btn_pressed():
	chosen_branch = BRANCH_1

func _on_Branch2Btn_pressed():
	chosen_branch = BRANCH_2

func _on_Cutscene2D_CHOICE(choice1, choice2):
	get_node("ChoiceBtnGrp/Branch1Btn").set_text(choice1)
	get_node("ChoiceBtnGrp/Branch2Btn").set_text(choice2)
	get_node("ChoiceBtnGrp").set_hidden(false)
	chosen_branch = BRANCH_1

func _on_Cutscene2D_BRANCH(branch, line):
	get_node("ChoiceBtnGrp").set_hidden(true)
	if (chosen_branch == int(branch)):
		_write_text(line)
	else:
		_on_NextBtn_pressed()

func _on_Cutscene2D_CHOICE_END(ignored):
	chosen_branch = BRANCH_0
