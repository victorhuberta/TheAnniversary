extends Node2D

signal game_over
signal save_game

signal preload_music
signal BGMUSIC
signal BGMUSIC_STOP
signal SOUND
signal BGSOUND
signal BGSOUND_STOP

var game_node_names = ["FindPairs", "MissingItem", "MemoryTrain", "MusicPlay"]
var game_nodes = []
var game_node_id = 0

var current_game
var game_timer
var game_time = 0
var n_completed_stages = 0

var tie
var script_timer

var script_state
const IDLE_STATE = -1
const NEED_TUTORIAL_STATE = 0
const WIN_STATE = 1
const LOSE_STATE = 2

var script
var s_index = 0

func _ready():
	_prepare_game_nodes()
	emit_signal("preload_music", "battle_theme")
	game_timer = get_node("GameTimer")

	tie = get_node("TextInterfaceEngine")
	script_timer = get_node("ScriptTimer")
	script = StringArray([])
	
	get_node("YesBtn").connect("pressed", self, "_say_yes")
	get_node("NoBtn").connect("pressed", self, "_say_no")
	get_node("ResetBtn").connect("pressed", self, "_reset")
	
func get_savedata():
	return game_node_id

func set_savedata(loaded_id):
	game_node_id = loaded_id

func load_savedata():
	next_game()

func _exit_tree():
	if (current_game != null):
		current_game.free()

func _prepare_game_nodes():
	for name in game_node_names:
		var game_node = load("res://scenes/%s.tscn" % name)
		game_nodes.append(game_node)

func _is_tutorial_needed():
	emit_signal("BGMUSIC", "battle_theme")
	_reset_script(current_game.opening)
	_next_script()
	script_state = NEED_TUTORIAL_STATE

func _next_script():
	tie.reset()
	if (s_index < script.size() and script[s_index] != ""):
		tie.buff_text(script[s_index], 0.01)
		tie.set_state(tie.STATE_OUTPUT)
		s_index += 1
	else:
		_start_game()

func _start_game():
	_reset_script(current_game.monologues)
	get_node("YesBtn").set_disabled(true)
	get_node("NoBtn").set_disabled(true)
	get_node("TimerLbl").set_hidden(false)
	game_time = current_game.GAME_TIME
	current_game.set_hidden(false)
	current_game.start_game()
	script_timer.start()
	game_timer.start()

func _on_ScriptTimer_timeout():
	var s_index = randi() % script.size()
	tie.reset()
	var text = script[s_index]
	if (text != ""):
		tie.buff_text(text, 0.01)
		tie.set_state(tie.STATE_OUTPUT)

func _say_yes():
	if script_state == WIN_STATE:
		emit_signal("BGMUSIC_STOP", null)
		emit_signal("BGSOUND_STOP", null)
		emit_signal("game_over")
		return
	elif script_state == LOSE_STATE:
		_reset()
		return
	script_state = IDLE_STATE
	_next_script()

func _say_no():
	if script_state == NEED_TUTORIAL_STATE:
		_reset_script(current_game.tutorial)
	else:
		_say_yes()
		return
	
	script_state = IDLE_STATE
	_next_script()

func _reset():
	emit_signal("BGSOUND_STOP", null)
	_reload_game_node()
	_prepare_for_reset()
	_is_tutorial_needed()

func next_game():
	emit_signal("save_game", "Interaction2D")
	_load_game_node()
	_prepare_for_reset()
	_is_tutorial_needed()

func _reset_script(new_script):
	s_index = 0
	script = StringArray([])
	script.append_array(new_script)

func _prepare_for_reset():
	script_timer.stop()
	game_timer.stop()
	current_game.set_hidden(true)
	get_node("YesBtn").set_disabled(false)
	get_node("NoBtn").set_disabled(false)
	get_node("TimerLbl").set_hidden(true)
	script_state = IDLE_STATE

func _load_game_node():
	if (game_node_id >= game_nodes.size()):
		game_node_id = 0

	current_game = game_nodes[game_node_id].instance()
	current_game.connect("game_won", self, "_current_game_won")
	current_game.connect("game_lost", self, "_current_game_lost")
	current_game.connect("special_req", self, "_handle_special_req")
	current_game.set_hidden(true)
	add_child(current_game)
	game_node_id += 1 # IMPORTANT!

func _reload_game_node():
	if (game_node_id <= 0): 
		return
	remove_child(current_game)
	game_node_id -= 1 # IMPORTANT!
	_load_game_node()

func _current_game_won():
	n_completed_stages += 1
	if (n_completed_stages < current_game.N_STAGES):
		_reload_game_node()
		_start_game()
	else:
		emit_signal("BGSOUND", "success")
		n_completed_stages = 0
		_reset_script(current_game.win_closing)
		_prepare_for_reset()
		script_state = WIN_STATE
		_next_script()

func _current_game_lost():
	emit_signal("BGSOUND", "icy_game_over")
	_reset_script(current_game.lose_closing)
	_prepare_for_reset()
	script_state = LOSE_STATE
	_next_script()

func _handle_special_req(req, param):
	emit_signal(req, param)

func _on_GameTimer_timeout():
	if (game_time <= 0):
		emit_signal("SOUND", "alarm")
		_current_game_lost()
		return
	game_time -= 1
	var minsec = _convert_to_minsec(game_time)
	var timerstr = "%02d:%02d" % [minsec[0], minsec[1]]
	get_node("TimerLbl").set_text(timerstr)

func _convert_to_minsec(game_time):
	return Vector2(game_time / 60, game_time % 60)