extends "res://code/MemoryGame.gd"

signal special_req

const GAME_TIME = 50
const N_STAGES = 1
const N_SUBSTAGES = 3
var n_completed_substages = 0

var music_notes = ["C", "C#", "D", "E", "Eb", "F", "F#", "G", "G#", "A", "B", "Bb"]
var chosen_notes = []
var n_chosen_notes = 1
var n_played_notes = 0
const N_DIFFICULTY_INCREASE = 2

var notes_btns

func _ready():
	seed(OS.get_unix_time())
	_load_scripts("musicplay")
	_init_notes_btns()

func _exit_tree():
	if (notes_btns != null):
		notes_btns.free()

func start_game():
	emit_signal("special_req", "BGMUSIC_STOP", null)
	_reset_game()

func _reset_game():
	n_chosen_notes += N_DIFFICULTY_INCREASE
	notes_btns.set_hidden(true)
	get_node("InstructionLbl").set_text("Listen carefully.")
	get_node("DelayTimer").start()

func _init_notes_btns():
	notes_btns = HButtonArray.new()
	notes_btns.set_pos(Vector2(280, 280))
	notes_btns.set_hidden(true)
	for note in music_notes:
		notes_btns.add_button(note)
	notes_btns.connect("button_selected", self, "_note_selected")
	add_child(notes_btns)

func _play_random_note():
	var choice = randi() % music_notes.size()
	chosen_notes.append(music_notes[choice])
	get_node("PianoPlayer").play(music_notes[choice])

func _on_SoundTimer_timeout():
	n_played_notes += 1
	if (n_played_notes > n_chosen_notes):
		n_played_notes = 0
		get_node("SoundTimer").stop()
		get_node("InstructionLbl").set_text("Repeat the music.")
		notes_btns.set_hidden(false)
		return
	_play_random_note()

func _note_selected(btn_idx):
	var note = notes_btns.get_button_text(btn_idx)
	get_node("PianoPlayer").play(note)

	if (! chosen_notes.has(note) or note != chosen_notes[0]):
		emit_signal("game_lost")
		return
	chosen_notes.remove(0)

	if (chosen_notes.size() == 0):
		n_completed_substages += 1
		if (n_completed_substages >= N_SUBSTAGES):
			n_completed_substages = 0
			emit_signal("game_won")
		else:
			_reset_game()

func _on_DelayTimer_timeout():
	get_node("SoundTimer").start()
