extends Node2D

signal game_won

var credits = preload("res://scenes/Credits.tscn")
var music_lib = {}
var sound_playing = false

func _ready():
	set_process_input(true)
	
func _input(event):
	if (event.is_action_pressed("cheat_skip") and not event.is_echo()):
		if (! get_node("Cutscene2D").is_hidden()):
			var target_idx = get_node("Cutscene2D").cutscene.size()
			while (get_node("Cutscene2D").cut_idx < target_idx):
				get_node("Cutscene2D")._on_NextBtn_pressed()
		elif (! get_node("Interaction2D").is_hidden()):
			emit_signal("game_won")

func _preload_music(musicname):
	var resname = "res://sounds/music/%s.ogg" % musicname
	music_lib[musicname] = load(resname)

func _play_bgsound(soundname):
	get_node("MusicPlayer").set_volume(0.0)
	get_node("SoundPlayer").play(soundname)
	sound_playing = true

func _stop_bgsound(ignored):
	get_node("SoundPlayer").stop_all()
	get_node("MusicPlayer").set_volume(1.0)
	get_node("MusicPlayer").set_volume_db(10)
	sound_playing = false

func _play_sound(soundname):
	if (sound_playing):
		return
	get_node("SoundPlayer").play(soundname)

func _play_bgmusic(musicname):
	if (! music_lib.has(musicname)):
		return
	get_node("MusicPlayer").set_stream(music_lib[musicname])
	get_node("MusicPlayer").set_volume_db(10)
	get_node("MusicPlayer").play()

func _stop_bgmusic(ignored):
	get_node("MusicPlayer").stop()

func _save_game(nodename):
	var savedict = {
		"current_node": nodename,
		"Interaction2D": get_node("Interaction2D").get_savedata(),
		"Cutscene2D": get_node("Cutscene2D").get_savedata()
	}
	var savefile = File.new()
	savefile.open("user://theanniversary.sav", File.WRITE)
	savefile.store_line(savedict.to_json())
	savefile.close()

func _load_game():
	var savefile = File.new()
	savefile.open("user://theanniversary.sav", File.READ)
	var savedata = {}
	savedata.parse_json(savefile.get_line())
	savefile.close()

	for key in savedata.keys():
		if (key == "current_node"):
			continue
		get_node(key).set_savedata(savedata[key])

	get_node(savedata["current_node"]).load_savedata()
	get_node(savedata["current_node"]).set_hidden(false)

func _on_Menu2D_new_game():
	remove_child(get_node("Menu2D"))
	get_node("Cutscene2D").set_hidden(false)
	get_node("Cutscene2D").play_story()

func _on_Menu2D_load_game():
	remove_child(get_node("Menu2D"))
	_load_game()

func _on_Interaction2D_game_over():
	get_node("Interaction2D").set_hidden(true)
	get_node("Cutscene2D").set_hidden(false)
	get_node("Cutscene2D").play_story()

func _on_Cutscene2D_cutscene_over():
	var cutscene2d = get_node("Cutscene2D")
	cutscene2d.set_hidden(true)
	if (cutscene2d.cutname_idx >= cutscene2d.cutscene_names.size()):
		add_child(credits.instance())
		return
	get_node("Interaction2D").set_hidden(false)
	get_node("Interaction2D").next_game()
