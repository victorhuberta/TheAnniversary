extends "res://code/MemoryGame.gd"

signal special_req

const GAME_TIME = 80
const N_STAGES = 1
const N_SUBSTAGES = 4
var n_completed_substages = 0

var buttons_arrays = []
var chosen_words = []
var n_chosen_words = 0
const N_DIFFICULTY_INCREASE = 2

func _ready():
	seed(OS.get_unix_time())
	_load_scripts("memorytrain")
	_init_buttons_arrays()
	_spawn_buttons_arrays()

func _exit_tree():
	for buttons_array in buttons_arrays:
		buttons_array.free()

func start_game():
	_reset_game()

func _init_buttons_arrays():
	var file = File.new()
	file.open("res://misc/words.txt", File.READ)
	var words = file.get_as_text().split("\n")
	file.close()

	var n_row_wc = 0
	var buttons_array = _create_buttons_array(0)
	var acc_y = 240
	for word in words:
		if (word == ""): continue
		buttons_array.add_button(word)
		n_row_wc += 1
		if (n_row_wc >= 8):
			buttons_array.set_pos(Vector2(240, acc_y))
			buttons_arrays.append(buttons_array)
			buttons_array = _create_buttons_array(buttons_arrays.size())
			n_row_wc = 0
			acc_y += 40

func _create_buttons_array(btnarr_idx):
	var buttons_array = HButtonArray.new()
	buttons_array.connect("button_selected", self, "_word_selected", [btnarr_idx])
	return buttons_array

func _spawn_random_words():
	chosen_words = []
	for _ in range(n_chosen_words):
		while (true):
			var btnarr_idx = randi() % buttons_arrays.size()
			var btn_idx = randi() % buttons_arrays[btnarr_idx].get_button_count()
			var chosen_word = buttons_arrays[btnarr_idx].get_button_text(btn_idx)
			if (! chosen_words.has(chosen_word)):
				chosen_words.append(chosen_word)
				break
	get_node("RandomWordsLbl").set_text(_group_words(chosen_words))

func _group_words(words):
	var group_words = ""
	for i in range(chosen_words.size()):
		if (i == (chosen_words.size() - 1)):
			group_words += ("and %s." % chosen_words[i])
		else:
			group_words += ("%s, " % chosen_words[i])
	return group_words

func _spawn_buttons_arrays():
	for buttons_array in buttons_arrays:
		add_child(buttons_array)

func _remove_buttons_arrays():
	for buttons_array in buttons_arrays:
		remove_child(buttons_array)

func _hide_buttons_arrays():
	for buttons_array in buttons_arrays:
		buttons_array.set_hidden(true)

func _show_buttons_arrays():
	for buttons_array in buttons_arrays:
		buttons_array.set_hidden(false)

func _word_selected(btn_idx, btnarr_idx):
	var selected_word = buttons_arrays[btnarr_idx].get_button_text(btn_idx)
	if (! chosen_words.has(selected_word) or chosen_words[0] != selected_word):
		emit_signal("game_lost")
		return
	chosen_words.remove(0)

	if (chosen_words.size() == 0):
		n_completed_substages += 1
		if (n_completed_substages >= N_SUBSTAGES):
			n_completed_substages = 0
			emit_signal("game_won")
		else:
			_reset_game()

func _reset_game():
	n_chosen_words += N_DIFFICULTY_INCREASE
	_hide_buttons_arrays()
	_spawn_random_words()
	get_node("RandomWordsLbl").set_hidden(false)
	get_node("StageTimer").start()

func _on_StageTimer_timeout():
	get_node("RandomWordsLbl").set_hidden(true)
	_show_buttons_arrays()
