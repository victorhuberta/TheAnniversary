extends Node2D

var credits = [
	"STORY\n\nVictor Huberta",
	"PROGRAMMING\n\nVictor Huberta",
	"GAME ART\n\nVictor Huberta\nOpen Game Art",
	"SOUND EFFECTS\n\nVictor Huberta\nOpen Game Art",
	"MUSIC\n\nVictor Huberta\nOpen Game Art",
	"SPECIAL THANKS TO\n\nGoogle Deep Style\nLunaPic\nmedia.io",
	"DEDICATED TO\n\nTheresia Coanata",
	"Happy anniversary. I love you with all my heart."
]
var cred_idx = 0

func _ready():
	pass

func next_credit():
	if (cred_idx < credits.size()):
		get_node("CreditsLbl").set_text(credits[cred_idx])
		cred_idx += 1

func last_credit():
	if (cred_idx >= credits.size()):
		get_node("CreditsAPlayer").stop()
		get_node("CreditsLbl").set_text(credits[cred_idx - 1])