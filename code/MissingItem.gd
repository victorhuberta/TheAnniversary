extends "res://code/MemoryGame.gd"

signal special_req

const GAME_TIME = 210
const N_STAGES = 3

var item_node = preload("res://scenes/Item.tscn")
var items = []
var item_choices = []
var n_unique_items
var missing_item

func _ready():
	seed(OS.get_unix_time())
	_load_scripts("missingitem")
	n_unique_items = item_node.instance().get_node("ItemASprite").\
		get_sprite_frames().get_frame_count("default")
	_prepare_item_choices()
	_generate_items()
	_spawn_items(items)

func _exit_tree():
	for item in items:
		item.free()

func _prepare_item_choices():
	for i in range(n_unique_items):
		var item = _create_item(i)
		item.connect("selected", self, "_choice_by_user")
		item_choices.append(item)

func _generate_items():
	for _ in range(n_unique_items * 3):
		var choice = randi() % n_unique_items
		var item = _create_item(choice)
		item.lock()
		items.append(item)

func _create_item(f_index):
	var item = item_node.instance()
	item.f_index = f_index
	item.get_node("ItemASprite").set_frame(f_index)
	return item

func _spawn_items(items_array):
	var screen_size = get_viewport_rect().size
	var item_size = item_node.instance().get_node("ItemBtn").get_disabled_texture().get_size()
	var acc_x = 80
	var acc_y = 200
	var items_copy = [] + items_array

	for _ in range(items_array.size()):
		var choice = randi() % items_copy.size()
		var item = items_copy[choice]
		item.set_pos(Vector2(acc_x, acc_y))
		add_child(item)
		acc_x += item_size.x + 90
		if (acc_x > screen_size.x):
			acc_x = 80
			acc_y += item_size.y + 40
		items_copy.remove(choice)

func _remove_items(items_array):
	for item in items_array:
		remove_child(item)

func _on_ReadyBtn_pressed():
	var missing_id = randi() % items.size()
	missing_item = items[missing_id]
	missing_item.set_hidden(true)
	get_node("ReadyBtn").set_hidden(true)
	_remove_items(items)
	_spawn_items(items)
	get_node("DisplayTimer").start()

func _choice_by_user(f_index):
	get_node("ChooseLbl").set_hidden(true)
	missing_item.set_hidden(false)
	_remove_items(item_choices)
	if (missing_item.get_node("ItemASprite").get_frame() == f_index):
		emit_signal("game_won")
	else:
		emit_signal("game_lost")

func _on_DisplayTimer_timeout():
	get_node("ChooseLbl").set_hidden(false)
	_remove_items(items)
	_spawn_items(item_choices)
