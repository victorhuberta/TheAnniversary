extends "res://code/MemoryGame.gd"

signal special_req

const GAME_TIME = 60
const N_STAGES = 1

var card_node = preload("res://scenes/Card.tscn")
var n_pairs = 12
var n_cards = n_pairs * 2
var card_faces = []
var cards = []

var prev_card = null # previously selected card

func _ready():
	seed(OS.get_unix_time())
	_load_scripts("findpairs")
	_generate_card_faces()
	_spawn_cards()

func _exit_tree():
	for card in cards:
		card.free()

func _generate_card_faces():
	for _ in range(n_pairs):
		card_faces.append(_get_rand_face())

func _get_rand_face():
	return floor(rand_range(1, card_node.instance().N_POSSIBLE_CARDS))

func _spawn_cards():
	var screen_size = get_viewport_rect().size
	var card_size = card_node.instance().get_node("CardBtn").get_disabled_texture().get_size()
	var acc_x = 80
	var acc_y = 120
	var faces_cnt = {}
	var tmp_faces = [] + card_faces # copy array
	while (tmp_faces.size() > 0):
		var card_face = tmp_faces[randi() % tmp_faces.size()]
		if (faces_cnt.has(card_face)):
			if (faces_cnt[card_face] >= 2):
				tmp_faces.erase(card_face)
				faces_cnt.erase(card_face)
				continue;
			else:
				faces_cnt[card_face] += 1
		else:
			faces_cnt[card_face] = 1
		var card_pos = Vector2(acc_x, acc_y)
		var card = _create_card(card_face, card_pos)
		cards.append(card)
		add_child(card)
		acc_x += card_size.x + 20
		if (acc_x > screen_size.x):
			acc_x = 80
			acc_y += card_size.y + 10

func _create_card(card_face, card_pos):
	var card = card_node.instance()
	card.face_up = card_face
	card.set_pos(card_pos)
	card.connect("selected", self, "_check_selected")
	return card

func _check_selected(selected_card):
	emit_signal("special_req", "SOUND", "draw")
	if (prev_card == null):
		prev_card = selected_card
		return

	if (prev_card == selected_card or prev_card.f_index == prev_card.FACE_DOWN):
		return

	if (prev_card.f_index == selected_card.f_index):
		prev_card.lock()
		selected_card.lock()
		prev_card = null
		_check_for_game_won()
	else:
		prev_card.toggle_card_face()
		prev_card = selected_card

func _check_for_game_won():
	for card in cards:
		if (! card.locked):
			return
	emit_signal("game_won")