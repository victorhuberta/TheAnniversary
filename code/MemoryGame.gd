extends Node2D

signal game_won
signal game_lost
const GAME_TIME = 60
const N_STAGES = 1

var opening
var monologues
var tutorial
var win_closing
var lose_closing

func _ready():
	pass

func start_game():
	pass

func _load_scripts(name):
	opening = _split_script_file(name, "%s_open" % name)
	monologues = _split_script_file(name, "%s_mono" % name)
	tutorial = _split_script_file(name, "%s_tut" % name)
	win_closing = _split_script_file(name, "%s_win" % name)
	lose_closing = _split_script_file(name, "%s_lose" % name)

func _split_script_file(dir, filename):
	var file = File.new()
	file.open("res://scripts/%s/%s.txt" % [dir, filename], File.READ)
	var script = file.get_as_text().split("\n")
	file.close()
	return script