extends Node2D

signal selected

const N_POSSIBLE_CARDS = 52
var face_up = 0
const FACE_DOWN = 0
var f_index = FACE_DOWN
var locked = false

func _ready():
	pass

func _on_CardBtn_pressed():
	toggle_card_face()

func toggle_card_face():
	if (locked):
		return

	if (f_index == FACE_DOWN):
		f_index = face_up
		emit_signal("selected", self)
	else:
		f_index = FACE_DOWN
	get_node("CardASprite").set_frame(f_index)

func lock():
	locked = true

func unlock():
	locked = false