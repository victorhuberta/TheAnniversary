extends Node2D

signal preload_music
signal BGMUSIC
signal BGMUSIC_STOP
signal new_game
signal load_game

const MENU_MUSIC = "snow_about_a_castle"

func _ready():
	emit_signal("preload_music", MENU_MUSIC)
	emit_signal("BGMUSIC", MENU_MUSIC)
	var file_check = File.new()
	if (file_check.file_exists("user://theanniversary.sav")):
		get_node("ContinueBtn").set_disabled(false)

func _on_NewGameBtn_pressed():
	emit_signal("BGMUSIC_STOP", null)
	emit_signal("new_game")

func _on_ContinueBtn_pressed():
	emit_signal("BGMUSIC_STOP", null)
	emit_signal("load_game")
