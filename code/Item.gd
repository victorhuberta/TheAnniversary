extends Node2D

signal selected

var f_index = 0
var locked = false

func _ready():
	pass

func lock():
	locked = true

func unlock():
	locked = false

func _on_ItemBtn_pressed():
	if (locked):
		return
	emit_signal("selected", f_index)
