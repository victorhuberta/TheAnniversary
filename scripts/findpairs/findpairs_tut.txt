The game you're going to play is called "Couple". From the rows of cards, you can only flip open two cards at any time.
If they happen to be a match, they stay opened. If they aren't a match, I will flip them back. Simple, right?
When they are facing down, I might change their positions in secret. But I'm probably too lazy for that. So, who knows?
Anyway, to win this game, all you have to do is to flip open all cards before the time runs out.
GOOD LUCK, kiddo!
